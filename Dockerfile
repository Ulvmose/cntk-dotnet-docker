FROM microsoft/cntk:2.6-cpu-python3.5
WORKDIR /root

LABEL maintainer "Alfred Ilberg Ulvmose alfred@ulvmose.su"

# Install apt-transport-https first because dotnet needs it
RUN apt-get update && apt-get install -y apt-transport-https

# Install Microsoft repos and run the first apt update
RUN wget -q https://packages.microsoft.com/config/ubuntu/16.04/packages-microsoft-prod.deb && \
    dpkg -i packages-microsoft-prod.deb && \
    apt-get update && \
    apt-get install -y dotnet-sdk-2.2 && \
    apt-get -y autoremove
